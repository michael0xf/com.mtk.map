These are reader, writer and logger as first examples and utilities for storage data. BaseFactoryImpl - an example of manipulations with data. First data examples see in '/res'.
MapThreads - this is other utility for work with threads and events, it's not used in OverlappingTreesPlainMap (mtkmap-*.*.*.jar) library, we use it for reader.
For any questions, for offers, and supporting please contact author.
E-mail: mtkravchenko@gmail.com
Skype: michael0xf
