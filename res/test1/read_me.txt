These are second examples of data file for simple uses of overlappingTreesPlainMap library. 
Source file "test.xml" contains tested tags "test", "test:1" which must rename to "test" while parsing and "second". 
In derived file "snapshot..." we can find "test:0" as block which competed in the namespace with other, to preserve the structure we added to it the index while writing.
The indents fail because of too difficult overlapping data.
