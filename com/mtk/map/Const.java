
package com.mtk.map;
/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public interface Const 
{
    //int SCRW = , SCRH = 320;
    public final static String ENC = "UTF-16LE";
    public final static String ENC2 = "UTF-8";

    int NIL = Integer.MIN_VALUE;
    float FNIL = Float.NaN;
    int ERROR = -1;
    int EOF = -1;    
//    int MAX3_LOAD_PRIORITY = 3, MAX2_LOAD_PRIORITY = 2, MAX1_LOAD_PRIORITY = 1, MEDIUM_LOAD_PRIORITY =0, MIN_LOAD_PRIORITY = -1;
    public String prpver = "109";
}


