package com.mtk.map;

import com.mtk.map.Cursor;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 1/23/17
 * Time: 12:41 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IStorage {
    public String getStorageString(Cursor cursor);
}
