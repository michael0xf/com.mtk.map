
package com.mtk.map;
/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public interface Container {
    public Object getObject();
}
