# com.mtk.map

This is Java library for working with overlapping trees both as with plain memory and as with hierarchical structure, with quick access to any levels and branches of tree.